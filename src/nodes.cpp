// 22: Psarska (302903), Pytel (302906), Rucka (302909)

#include "nodes.hpp"

void Worker::do_work(Time time_) {

    if (!buffor_) {
        if( cend() != cbegin()) {
            buffor_ = queue_->pop();
            start_time_ = time_;
        }
    }

    if( time_ == start_time_ + processing_duration_ - 1) {
        push_package(std::move(*buffor_));
        buffor_.reset();
    }
}


void Ramp::deliver_goods(Time t) {
    if ((t-1) % _di == 0) {
        Package new_package;
        push_package(std::move(new_package));
    }
}


void ReceiverPreferences::add_receiver(IPackageReceiver* new_receiver) {
    if(preferences_.empty()){
        preferences_.insert(std::pair<IPackageReceiver*, double>(new_receiver, 1.0));
    }
    else {
        double size = preferences_.size() + 1.0;
        for (auto &pref : preferences_) {
            pref.second = pref.second * (1.0 - (1.0 / size));
        }
        preferences_.insert(std::pair<IPackageReceiver *, double>(new_receiver, 1.0 / size));
    }
}


void ReceiverPreferences::remove_receiver(IPackageReceiver* deleted_receiver) {
    double delprob = preferences_[deleted_receiver];
    preferences_.erase(deleted_receiver);
    for (auto& pref : preferences_) {
        pref.second /= (1-delprob);
    }
}


IPackageReceiver* ReceiverPreferences::choose_receiver() const {
    double distribution = 0.0;
    double d_probability = pg_();
    IPackageReceiver* remember;
    for (auto& pref : preferences_) {
        distribution += pref.second;
        remember = pref.first;
        if (d_probability <= distribution) {
            return pref.first;
        }
    }
    return remember;
}


const ReceiverPreferences::preferences_t& ReceiverPreferences::get_preferences() const {
    return preferences_;
}

void PackageSender::send_package() {
    if(_sending_buffer) {
        auto receiver = receiver_preferences_.choose_receiver();
        receiver->receive_package(std::move(*_sending_buffer));
        _sending_buffer.reset();
    }
}

void PackageSender::push_package(Package && p) {
    _sending_buffer.emplace(std::move(p));
}

const std::optional<Package>& PackageSender::get_sending_buffer() const {
   return _sending_buffer;
}
// 22: Psarska (302903), Pytel (302906), Rucka (302909)