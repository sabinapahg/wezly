// 22: Psarska (302903), Pytel (302906), Rucka (302909)
#include "helpers.hpp"

std::random_device rd;
std::mt19937 rng(rd());

double default_probability_generator() {
    return std::generate_canonical<double, 10>(rng);
}

ProbabilityGenerator probability_generator = default_probability_generator;

// 22: Psarska (302903), Pytel (302906), Rucka (302909)