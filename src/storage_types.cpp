// 22: Psarska (302903), Pytel (302906), Rucka (302909)
#include "storage_types.hpp"

Package PackageQueue::pop() {
    Package result;
    switch(_type) {
        case PackageQueueType::FIFO: {
            Package pack =  std::move(_list_of_packages.front());
            _list_of_packages.pop_front();
            result = std::move(pack);
            break;
        }

        case PackageQueueType::LIFO: {
            Package pack = std::move( _list_of_packages.back());
            _list_of_packages.pop_back();
            result = std::move(pack);
            break;
        }
    }
    return result;
}
// 22: Psarska (302903), Pytel (302906), Rucka (302909)