// 22: Psarska (302903), Pytel (302906), Rucka (302909)
#ifndef NETSIM_PACKAGE_HPP
#define NETSIM_PACKAGE_HPP

#include "types.hpp"

#include <set>
#include <deque>
#include <list>

class Package {
public:
    Package();

    explicit Package(ElementID id);

    Package(const Package&) = delete;
    Package(Package&&) noexcept;

    Package& operator=(Package&) = delete;
    Package& operator=(Package&&) noexcept;

    ~Package();

    ElementID get_id() const { return id_; }

private:
    static const ElementID BLANK_ID = -1;

    inline static std::set<ElementID> assigned_ids_{0};

    inline static std::set<ElementID> freed_ids_;

    ElementID id_ = BLANK_ID;
};

#endif //NETSIM_PACKAGE_HPP
// 22: Psarska (302903), Pytel (302906), Rucka (302909)