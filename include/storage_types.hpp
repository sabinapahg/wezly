// 22: Psarska (302903), Pytel (302906), Rucka (302909)
#ifndef NETSIM_STORAGE_TYPES_HPP
#define NETSIM_STORAGE_TYPES_HPP

#include "package.hpp"


enum class PackageQueueType {
    FIFO, LIFO
};


class IPackageStockpile {
public:
    using  const_iterator = std::list<Package>::const_iterator;

    virtual const_iterator begin() = 0;
    virtual const_iterator end() = 0;
    virtual const_iterator cbegin() const = 0;
    virtual const_iterator cend() const = 0;

    virtual void push(Package&&) = 0;
    virtual bool empty() const= 0;
    virtual size_type size() const= 0;
};


class IPackageQueue : public IPackageStockpile {
public:
    virtual Package pop() = 0;
    virtual PackageQueueType get_queue_type() const = 0;
};


class PackageQueue : public IPackageQueue {
public:
    explicit PackageQueue(PackageQueueType type) : _list_of_packages(), _type(type) {}

    void push(Package&& package) override { _list_of_packages.emplace_back(std::move(package)); }
    bool empty() const override { return _list_of_packages.empty(); }
    size_type size() const override { return _list_of_packages.size(); }
    Package pop() override;

    PackageQueueType get_queue_type() const override { return _type; }

    const_iterator cbegin() const override { return _list_of_packages.cbegin(); }
    const_iterator cend() const override { return _list_of_packages.cend(); }
    const_iterator begin() override { return _list_of_packages.begin(); }
    const_iterator end() override { return _list_of_packages.end(); }

private:
    std::list<Package> _list_of_packages;
    PackageQueueType _type;
};

#endif //NETSIM_STORAGE_TYPES_HPP
// 22: Psarska (302903), Pytel (302906), Rucka (302909)