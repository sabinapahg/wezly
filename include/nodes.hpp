// 22: Psarska (302903), Pytel (302906), Rucka (302909)
#ifndef NETSIM_NODES_HPP
#define NETSIM_NODES_HPP

#include "storage_types.hpp"
#include "helpers.hpp"

#include <memory>
#include <optional>
#include <map>
#include <iostream>


class IPackageReceiver {
public:
    virtual void receive_package(Package&&)  = 0;
    virtual ElementID get_id() const  = 0;

    using  const_iterator = std::list<Package>::const_iterator;

    virtual const_iterator begin() const = 0;
    virtual const_iterator end() const = 0;
    virtual const_iterator cbegin() const = 0;
    virtual const_iterator cend() const = 0;

};



class ReceiverPreferences {
public:
    using preferences_t = std::map<IPackageReceiver*, double>;
    using const_iterator = preferences_t::const_iterator;

    explicit ReceiverPreferences(ProbabilityGenerator pg = probability_generator) : pg_(std::move(pg)) {};

    void add_receiver(IPackageReceiver* new_receiver);
    void remove_receiver(IPackageReceiver* deleted_receiver);
    IPackageReceiver* choose_receiver() const ;
    const preferences_t& get_preferences() const ;

    const_iterator begin() const  { return preferences_.cbegin(); }
    const_iterator end()  const { return preferences_.cend(); }
    const_iterator cbegin() const { return preferences_.cbegin(); }
    const_iterator cend() const { return preferences_.cend(); }

private:
    preferences_t preferences_;
    ProbabilityGenerator pg_;
};



class PackageSender {
protected:
    std::optional<Package> _sending_buffer;

public:
    ReceiverPreferences receiver_preferences_;

    explicit PackageSender() : _sending_buffer(), receiver_preferences_() {}; //
    virtual ~PackageSender() = default;
    PackageSender(PackageSender&&) = default;

    void send_package();
    void push_package(Package && p);
    const std::optional<Package> & get_sending_buffer() const;

};



class Storehouse : public IPackageReceiver {
public:
    Storehouse(ElementID id, std::unique_ptr<IPackageStockpile> d = std::make_unique<PackageQueue>(PackageQueue(PackageQueueType::FIFO))): storehouse_id_(id), d_(std::move(d)) {};

    void receive_package(Package&& p) override { d_->push(std::move(p)); }
    ElementID get_id() const override { return storehouse_id_; }

    const_iterator begin() const  override { return d_->cbegin(); }
    const_iterator end() const override { return d_->cend(); }
    const_iterator cbegin() const override { return d_->cbegin(); }
    const_iterator cend() const override { return d_->cend(); }

private:
    ElementID storehouse_id_;
    std::unique_ptr<IPackageStockpile> d_;
};



class Worker : public PackageSender, public IPackageReceiver {
public:
    Worker(ElementID id, TimeOffset pd, std::unique_ptr<IPackageQueue> q): PackageSender(), worker_id_(id), processing_duration_(pd), queue_(std::move(q)) {};

    void do_work(Time time_);
    TimeOffset get_processing_duration() const { return processing_duration_; }
    Time get_package_processing_start_time() const { return start_time_; }

    void receive_package(Package&& p) override { queue_->push(std::move(p)); }
    ElementID get_id() const override { return worker_id_; }

    const_iterator begin() const override { return queue_->cbegin(); }
    const_iterator end() const override { return queue_->cend(); }
    const_iterator cbegin() const override { return queue_->cbegin(); }
    const_iterator cend() const override { return queue_->cend(); }

private:
    ElementID worker_id_;
    TimeOffset processing_duration_;
    std::unique_ptr<IPackageQueue> queue_;
    std::optional<Package> buffor_;
    Time start_time_;
};



class Ramp : public PackageSender {
public:
    Ramp(ElementID id, TimeOffset di) : PackageSender(), _id(id), _di(di) {};

    void deliver_goods(Time t);

    TimeOffset get_delivery_interval() const { return _di; }

    ElementID get_id() const { return _id; }

private:
    ElementID _id;
    TimeOffset _di;
};

#endif //NETSIM_NODES_HPP
// 22: Psarska (302903), Pytel (302906), Rucka (302909)