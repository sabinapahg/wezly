// 22: Psarska (302903), Pytel (302906), Rucka (302909)
#ifndef NETSIM_TYPES_HPP
#define NETSIM_TYPES_HPP

#include <functional>

typedef int ElementID;
typedef int size_type;
typedef int Time;
typedef int TimeOffset;
using ProbabilityGenerator = std::function<double()>;

#endif //NETSIM_TYPES_HPP
// 22: Psarska (302903), Pytel (302906), Rucka (302909)