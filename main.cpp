
#include <iostream>
#include "nodes.hpp"
#include "package.hpp"
#include "nodes.hpp"
#include "storage_types.hpp"
#include "helpers.hpp"


int main() {


/*

        PackageQueue q1(PackageQueueType::FIFO);
        PackageQueue q2(PackageQueueType::LIFO);
        PackageQueue q3(PackageQueueType::FIFO);
        PackageQueue q4(PackageQueueType::LIFO);
        PackageQueue q5(PackageQueueType::LIFO);

        std::unique_ptr<IPackageQueue> ptr1 = std::make_unique<PackageQueue>(q1);
        std::unique_ptr<IPackageQueue> ptr2 = std::make_unique<PackageQueue>(q2);
        std::unique_ptr<IPackageStockpile> ptr3 = std::make_unique<PackageQueue>(q3);
        std::unique_ptr<IPackageStockpile> ptr4 = std::make_unique<PackageQueue>(q4);
        std::unique_ptr<IPackageStockpile> ptr5 = std::make_unique<PackageQueue>(q5);
        Storehouse s1(1, std::move(ptr3));
        Storehouse s2(2, std::move(ptr4));
        Storehouse s3(3, std::move(ptr5));
        Ramp r1(1, 1);
        Ramp r2(2, 2);
        Worker w1(1, 1, std::move(ptr1));
        Worker w2(2, 2, std::move(ptr2));
        //-----------------------------------------------
        // make your own preferences
        //e.x. r1.mReceiverPreferences.add_receiver(&w1);
        //-----------------------------------------------
        //std::function<double(void)> rng = probability_generator;
        r1.receiver_preferences_.add_receiver(&s1)  ;
        r1.receiver_preferences_.add_receiver(&s2);
        r1.receiver_preferences_.add_receiver(&w1);
//        pref.add_receiver(&w2);
//        pref.add_receiver(&s3);





        std::cout<<r1.receiver_preferences_.choose_receiver()<<std::endl;
        std::cout<< &w1<<std::endl;
        std::cout<< &w2<<std::endl;
        std::cout<< &s1<<std::endl;
        std::cout<< &s2<<std::endl;

        //cpi->begin();






    Ramp apl(1,12);
    TimeOffset di = apl.get_delivery_interval();
    apl.deliver_goods(12);
    std::cout<<di<<std::endl;



    PackageQueue p(PackageQueueType::FIFO);
    p.push(Package());
    p.push(Package());
    p.push(Package());

    Package test = p.pop();
    std::cout<< test.get_id()<<std::endl;
    test = p.pop();
    std::cout<< test.get_id()<<std::endl;
    test = p.pop();
    std::cout<< test.get_id()<<std::endl;
    std::cout<< p.size() <<std::endl;
    std::cout<< p.empty() <<std::endl;




//    packagesender test

//    r1.deliver_goods()



*/





//TEST1
/*
    std::unique_ptr<IPackageQueue> ptr1 = std::make_unique<PackageQueue>(PackageQueue(PackageQueueType::FIFO));
    std::unique_ptr<IPackageQueue> ptr2 = std::make_unique<PackageQueue>(PackageQueue(PackageQueueType::LIFO));
    std::unique_ptr<IPackageStockpile> ptr3 = std::make_unique<PackageQueue>(PackageQueue(PackageQueueType::FIFO));
    std::unique_ptr<IPackageStockpile> ptr4 = std::make_unique<PackageQueue>(PackageQueue(PackageQueueType::LIFO));
    Storehouse s1(1, std::move(ptr3));
    Storehouse s2(2, std::move(ptr4));
    Ramp r1(1, 1);
    Ramp r2(2, 2);
    Worker w1(1, 1, std::move(ptr1));
    Worker w2(2, 2, std::move(ptr2));
    //-----------------------------------------------
    // make your own preferences
    //e.x. r1.mReceiverPreferences.add_receiver(&w1);
    r1.receiver_preferences_.add_receiver(&w1);
    //-----------------------------------------------
    // test logic
    r1.deliver_goods(0);
    r1.send_package();

    if (std::nullopt == r1.get_sending_buffer()) std::cout << "1 w 1t\n" ;
    else std::cout << "0 w 1t\n";
*/

//TEST2
/*
    std::unique_ptr<IPackageQueue> ptr5 = std::make_unique<PackageQueue>(PackageQueue(PackageQueueType::FIFO));
    std::unique_ptr<IPackageStockpile> ptr6 = std::make_unique<PackageQueue>(PackageQueue(PackageQueueType::FIFO));
    Worker w(1, 2,std::move(ptr5));
    Ramp r(1,2);
    Storehouse s(1, std::move(ptr6));
    r.receiver_preferences_.add_receiver(&w);
    w.receiver_preferences_.add_receiver(&s);
    r.deliver_goods(1);
    r.send_package();
    w.do_work(1);
    w.send_package();
    r.deliver_goods(2);
    r.send_package();
    w.do_work(2);
    w.send_package();
    r.deliver_goods(3);
    r.send_package();
    w.do_work(3);
    w.send_package();

    if (1 == s.cbegin()->get_id()) std::cout << "1 w 2t\n" ;
    else std::cout << "0 w 2t\n";

    */

//TEST3
/*
    std::unique_ptr<IPackageQueue> ptr1 = std::make_unique<PackageQueue>(PackageQueue(PackageQueueType::FIFO));
    std::unique_ptr<IPackageQueue> ptr2 = std::make_unique<PackageQueue>(PackageQueue(PackageQueueType::LIFO));
    std::unique_ptr<IPackageStockpile> ptr3 = std::make_unique<PackageQueue>(PackageQueue(PackageQueueType::FIFO));
    std::unique_ptr<IPackageStockpile> ptr4 = std::make_unique<PackageQueue>(PackageQueue(PackageQueueType::LIFO));
    Storehouse s1(1, std::move(ptr3));
    Storehouse s2(2, std::move(ptr4));
    Ramp r1(1, 1);
    Ramp r2(2, 2);
    Worker w1(1, 1, std::move(ptr1));
    Worker w2(2, 2, std::move(ptr2));
    //-----------------------------------------------
    // make your own preferences
    //e.x. r1.mReceiverPreferences.add_receiver(&w1);
    r2.receiver_preferences_.add_receiver(&w1);
    r2.deliver_goods(1);
    r2.send_package();
    r2.deliver_goods(2);
    r2.send_package();
    r2.deliver_goods(3);

    if (1 == r2.get_sending_buffer().has_value()) std::cout << "1 w 3t\n" ;
    else std::cout << "0 w 3t\n";
*/

//TEST4
/*
    std::unique_ptr<IPackageQueue> ptr1 = std::make_unique<PackageQueue>(PackageQueue(PackageQueueType::FIFO));
    std::unique_ptr<IPackageQueue> ptr2 = std::make_unique<PackageQueue>(PackageQueue(PackageQueueType::LIFO));
    std::unique_ptr<IPackageStockpile> ptr3 = std::make_unique<PackageQueue>(PackageQueue(PackageQueueType::FIFO));
    std::unique_ptr<IPackageStockpile> ptr4 = std::make_unique<PackageQueue>(PackageQueue(PackageQueueType::LIFO));
    Storehouse s1(1, std::move(ptr3));
    Storehouse s2(2, std::move(ptr4));
    Ramp r1(1, 1);
    Ramp r2(2, 2);
    Worker w1(1, 1, std::move(ptr1));
    Worker w2(2, 2, std::move(ptr2));
    //-----------------------------------------------
    // make your own preferences
    //e.x. r1.mReceiverPreferences.add_receiver(&w1);
    r2.receiver_preferences_.add_receiver(&w1);
    r2.deliver_goods(1);
    r2.send_package();
    r2.deliver_goods(2);

    if (0 == r2.get_sending_buffer().has_value()) std::cout << "1 w 4t\n" ;
    else std::cout << "0 w 4t\n";

    */

//TEST5
/*
   std::unique_ptr<IPackageQueue> ptr = std::make_unique<PackageQueue>(PackageQueue(PackageQueueType::FIFO));
    Storehouse s(1, std::move(ptr));
    Ramp r(1,2);
    r.receiver_preferences_.add_receiver(&s);
    r.deliver_goods(1);
    r.send_package();
    r.deliver_goods(2);
    r.send_package();
    r.deliver_goods(3);
    r.send_package();

    if (2 == s.cend()->get_id()) std::cout << "1 w 5t\n" ;
    else std::cout << "0 w 5t\n";
*/

//TEST6
/*
    std::unique_ptr<IPackageQueue> ptr1 = std::make_unique<PackageQueue>(PackageQueue(PackageQueueType::FIFO));
    std::unique_ptr<IPackageQueue> ptr2 = std::make_unique<PackageQueue>(PackageQueue(PackageQueueType::LIFO));
    std::unique_ptr<IPackageStockpile> ptr3 = std::make_unique<PackageQueue>(PackageQueue(PackageQueueType::FIFO));
    std::unique_ptr<IPackageStockpile> ptr4 = std::make_unique<PackageQueue>(PackageQueue(PackageQueueType::LIFO));
    Storehouse s1(1, std::move(ptr3));
    Storehouse s2(2, std::move(ptr4));
    Ramp r1(1, 1);
    Ramp r2(2, 2);
    Worker w1(1, 1, std::move(ptr1));
    Worker w2(2, 2, std::move(ptr2));
    //-----------------------------------------------
    // make your own preferences
    //e.x. r1.mReceiverPreferences.add_receiver(&w1);
    //-----------------------------------------------
    std::function<double(void)> rng = your_num;
    ReceiverPreferences pref(rng);

    IPackageReceiver* rec1;
    IPackageReceiver* rec2;

    rec1 = &w1;
    rec2 = &w2;

    pref.add_receiver(&w1);
    pref.add_receiver(&w2);

    if (__min(rec1, rec2) == pref.choose_receiver()) std::cout << "1 w 6t\n" ;
    else std::cout << "0 w 6t\n";
*/



    return 0;
}
